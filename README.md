# Socat Ethernet Serial Server

This repository contains a Systemd service which uses Socat to implement a multiport Ethernet serial server.

## Installation

Install the Systemd unit files:

``` shell
    cp netcom@.service netcom.target /etc/systemd/service
    systemctl daemon-reload
```

For each serial port you wish to connect, create a file named `/etc/default/netcom-`*device*, where *device* is the serial port device name, e.g. `/etc/default/netcom-ttyUSB0`. This file must contain two environment variable settings.

``` shell
    cat<<EOF > /etc/default/netcom-ttyUSB0
    # TCP port
    PORT=4001
    # Serial baud rate
    BAUD=115200
    EOF
```

Enable and start the service:

``` shell
    systemctl enable netcom.target
    systemctl enable netcom@ttyUSB0.service
    systemctl start netcom@ttyUSB0.service
```

Now connecting to TCP port 4001 on the host will open a 115200 (N81) connection to the serial device `/dev/ttyUSB0`
